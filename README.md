WIFI Fix (No Wifi Adapter Found)
1.  Tether to phone internet connection through USB
2.  `sudo apt-get install --reinstall bcmwl-kernel-source`
3.  https://support.killernetworking.com/knowledge-base/killer-ax1650-in-debian-ubuntu-16-04/

After power loss Wifi adapter could not be found (fix below)
https://askubuntu.com/questions/1016903/alienware-17-r4-ubuntu-16-04-wifi-driver

Backlight Control

Create /usr/local/bin/xbacklightmon.sh

```
#!/bin/bash

# modify this path to the location of your backlight class
path=/sys/class/backlight/dell_backlight

luminance() {
    read -r level < "$path"/actual_brightness
    factor=$((max))
    new_brightness="$(bc -l <<< "scale = 2; $level / $factor")"
    printf '%f\n' $new_brightness
}

read -r max < "$path"/max_brightness

xrandr --output eDP1 --brightness "$(luminance)"

inotifywait -me modify --format '%w' "$path"/actual_brightness | while read; 
do     
    xrandr --output eDP1 --brightness "$(luminance)"
done
```

Autostart watcher

Create ~/.config/autostart/xbacklightmon.desktop

```
    [Desktop Entry]
    Type=Application
    Exec=/usr/local/bin/xbacklightmon.sh
    Hidden=false
    X-GNOME-Autostart-enabled=true
    Name=xbacklightmon
```